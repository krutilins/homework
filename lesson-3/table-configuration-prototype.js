/**
 * 
 */
function Grid () {
  this.tableName = "default",
  this.isScrolAvailable = false,
  this.backgroundColor = "blue"
};
Grid.prototype.setTableName = function (tableName) {
  this.tableName = tableName;
};
Grid.prototype.setIsScrolAvailable = function (isScrolAvailable) {
  this.isScrolAvailable = isScrolAvailable;
};
Grid.prototype.setBackgroundColor = function (backgroundColor) {
  this.backgroundColor = backgroundColor;
};
Grid.prototype.configureTable = function (tableName, isScrolAvailable, backgroundColor) {
  this.setTableName(tableName);
  this.setIsScrolAvailable(isScrolAvailable);
  this.setBackgroundColor(backgroundColor);
};
Grid.prototype.logInfo = function () {    
  console.log(
    'tableName:',`${this.tableName}`,
    '\nisScrolAvailable:', `${this.isScrolAvailable}`,
    '\nbackgroundColor:',`${this.backgroundColor}`
  );
};
/**
 * 
 */
function User () {
  this.columnsAmount = 3;
  this.rowsAmount = 0;
  this.userData = [];
  this.userId = 0;
}
User.prototype = Object.create(Grid.prototype);
User.prototype.constructor = User; 
User.prototype.addUser = function (fullName, email) {
  this.userData.push({id: this.userId, fullName, email});
  this.rowsAmount++;
  this.userId++;
};
User.prototype.deleteUserById = function (id) {
  this.userData.forEach((item, i) => {
    if (item.id === id) {
      this.deleteUserByIndex(i);
    }
  }, this)
};
User.deleteUserByIndex = function (index) {
  this.userData.splice(index, 1);
  this.rowsAmount--;
};
User.prototype.clearTable = function () {
  this.userData = [];
  this.rowsAmount = 0;
  this.userId = 0;
};
User.prototype.getUserData = function () {
  return this.userData;
};
User.prototype.configureTable = function (tableName, isScrolAvailable, backgroundColor, borderColor) {
  Grid.prototype.configureTable.call(this, tableName, isScrolAvailable, backgroundColor);
  this.borderColor = borderColor;
};
User.prototype.logInfo = function () {
  let resultLog = ``;
  this.userData.forEach((item) => {
    resultLog += `${item.id} ${item.fullName} ${item.email} \n`;
  });
  console.log(resultLog);
};
/**
 * 
 */
const NOT_SELECTED = -1;
function Oder () {
  this.columnsAmount = 3;
  this.rowsAmount = 0;
  this.oderData = [];
  this.oderId = 0;
  this.checkboxIndex = NOT_SELECTED;
}
Oder.prototype = Object.create(Grid.prototype);
Oder.prototype.constructor = Oder; 
Oder.prototype.addOder = function (product) {
  this.oderData.push({id: this.oderId, product, isCheckbox: false});
  this.rowsAmount++;
  this.oderId++;
};
Oder.prototype.deleteOderById = function (id) {
  if (this.userData[this.checkboxIndex].id === id) {
    this.deleteOderByIndex(this.checkboxIndex);
    this.checkboxIndex = NOT_SELECTED;
  } else {
    this.oderData.forEach((item, i) => {
      if (item.id === id) {
        this.deleteOderByIndex(i);
      }
    }, this);
  }
};
Oder.deleteOderByIndex = function (index) {
  this.oderData.splice(index, 1);
  this.rowsAmount--;
};
Oder.prototype.clearTable = function () {
  this.oderData = [];
  this.rowsAmount = 0;
  this.oderId = 0;
  this.checkboxIndex = NOT_SELECTED;
};
Oder.prototype.setCheckboxById = function (id) {
  this.oderData = this.oderData.map((item, i) => {
    if (item.id === id) {
      if (this.checkboxIndex !== NOT_SELECTED){
          this.oderData[this.checkboxIndex].isCheckbox = false;
      }
      item.isCheckbox = true;
      this.checkboxIndex = i;
    }
    return item;
  });
};
Oder.prototype.getOderData = function () {
  return this.oderData;
};
Oder.prototype.configureTable = function (tableName, isScrolAvailable, backgroundColor, borderColor) {
  Grid.prototype.configureTable.call(this, tableName, isScrolAvailable, backgroundColor);
  this.borderColor = borderColor;
};
Oder.prototype.logInfo = function () {
  let resultLog = ``;
  this.oderData.forEach((item) => {
    resultLog += `${item.id} ${item.product} ${item.isCheckbox} \n`;
  })
  console.log(resultLog);
};