/**
 * 
 */
class Grid {
  constructor() {
    this.tableName = "default";
    this.isScrolAvailable = false;
    this.backgroundColor = "blue";
  }
  setTableName(tableName) {
    this.tableName = tableName;
  }
  setIsScrolAvailable(isScrolAvailable) {
    this.isScrolAvailable = isScrolAvailable;
  }
  setBackgroundColor(backgroundColor) {
    this.backgroundColor = backgroundColor;
  }
  configureTable (tableName, isScrolAvailable, backgroundColor) {
    this.setTableName(tableName);
    this.setIsScrolAvailable(isScrolAvailable);
    this.setBackgroundColor(backgroundColor);
  }
  logInfo () {
    console.log(
      'tableName:',`${this.tableName}`,
      '\nisScrolAvailable:', `${this.isScrolAvailable}`,
      '\nbackgroundColor:',`${this.backgroundColor}`
    );
  }
}
/**
 * 
 */
class User extends Grid {
  constructor() {
    super();
    this.columnsAmount = 3;
    this.rowsAmount = 0;
    this.userData = [];
    this.userId = 0;
  }
  addUser (fullName, email) {
    this.userData.push({id: this.userId, fullName, email});
    this.rowsAmount++;
    this.userId++;
  }
  deleteUserById (id) {
    this.userData.forEach((item, i) => {
      if (item.id === id) {
        this.deleteUserByIndex(i);
      }
    }, this);
  }
  deleteUserByIndex (index) {
    this.userData.splice(index, 1);
    this.rowsAmount--;
  }
  clearTable () {
    this.userData = [];
    this.rowsAmount = 0;
    this.userId = 0;
  }
  getUserData () {
    return this.userData;
  }
  configureTable(tableName, isScrolAvailable, backgroundColor, borderColor) {
    super.configureTable(tableName, isScrolAvailable, backgroundColor);
    this.borderColor = borderColor;
  }
  logInfo () {
    let resultLog = ``;
    this.userData.forEach((item) => {
      resultLog += `${item.id} ${item.fullName} ${item.email} \n`;
    });
    console.log(resultLog);
  }
}
/**
 * 
 * 
 */
const NOT_SELECTED = -1;
class Oder extends Grid {
  constructor() {
    super()
    this.columnsAmount = 3;
    this.rowsAmount = 0;
    this.oderData = [];
    this.oderId = 0;
    this.checkboxIndex = NOT_SELECTED;
  }
  addOder (product) {
    this.oderData.push({id: this.oderId, product, isCheckbox: false});
    this.rowsAmount++;
    this.oderId++;
  }
  deleteOderById (id) {
    if (this.userData[this.checkboxIndex].id === id) {
      this.deleteOderByIndex(this.checkboxIndex);
      this.checkboxIndex = NOT_SELECTED;
    } else {
      this.oderData.forEach((item, i) => {
        if (item.id === id) {
          this.deleteOderByIndex(i);
        }
      }, this);
    }
  }
  deleteUserByIndex(index) {
    this.oderData.splice(index, 1);
    this.rowsAmount--;
  }
  clearTable () {
    this.oderData = [];
    this.rowsAmount = 0;
    this.oderId = 0;
    this.checkboxIndex = NOT_SELECTED;
  }
  setCheckboxById (id) {
    this.oderData = this.oderData.map((item, i) => {
      if (item.id === id) {
        if (this.checkboxIndex !== NOT_SELECTED){
            this.oderData[this.checkboxIndex].isCheckbox = false;
        }
        item.isCheckbox = true;
        this.checkboxIndex = i;
      }
      return item;
    });
  }
  getOderData () {
    return this.oderData;
  }
  configureTable(tableName, isScrolAvailable, backgroundColor, borderColor) {
    super.configureTable(tableName, isScrolAvailable, backgroundColor);
    this.borderColor = borderColor;
  }
  logInfo() {
    let resultLog = ``;
    this.oderData.forEach((item) => {
      resultLog += `${item.id} ${item.product} ${item.isCheckbox} \n`;
    })
    console.log(resultLog);
  }
}