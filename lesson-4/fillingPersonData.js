const ADMIN = 'admin';
const USER = 'user';
const GUEST = 'guest';

const personData = {};

function getGroupSize(ID) {
  const groupSize = {
    1: 4,
    2: 6,
    3: 2
  }
  return groupSize[ID];
}
new Promise((resolve) => {
  setTimeout(() => {
    resolve('moon'); 
  }, 1000);
}).then((location) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      personData.location = location;
      resolve(ADMIN);
    }, 4000);
  });
}).then((accessLevel) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const username = 'vobobobo';
      personData.accessLevel = accessLevel;
      personData.email = username.concat(`@${accessLevel}.com`);
      if (personData.accessLevel === ADMIN) {
        resolve(3);
      } else {
        reject(new Error('you have no access, you will not pass'));
      }
    }, 2000);
  });
}).then((ID) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      personData.ID = ID;
      personData.groupSize = getGroupSize(ID);
      resolve(['Саша', 'Влад', 'Юля', 'Андрей', 'Богдан']);
    }, 1000);
  });
}).then((group) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (group.length > personData.groupSize) {
        reject(new Error('The group is crowded'));
      } else {
        personData.group = group;
        resolve();
      }
    }, 1000);
  });
}).then(() => {
  console.log(personData);
}).catch(console.error)