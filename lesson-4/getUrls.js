function request(url) {
	return new Promise((res, rej) => {
    const delayTime = Math.floor(Math.random() * 10000) + 1;

    setTimeout(() => res(url), delayTime);
  });
}
function getUrls(urls) {
	return new Promise(resolve => {
		urls.map(request).reduce(async (resultP, promise, index, promises) => {
			let { result = [], count = 0 } = await resultP;
			result[index] = await promise;
			if (promises.length === ++count) {
				resolve(result)
			};
			return { result, count };
		}, {});
	});
}