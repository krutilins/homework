const CITIES = [
  {"id": 833},{"id": 2960},{"id": 3245},{"id": 3530},
  {"id": 5174},{"id": 7264},{"id": 8084},{"id": 9874},
  {"id": 11263},{"id": 11754},{"id": 12795},{"id": 14177},
  {"id": 14256},{"id": 18007},{"id": 18093},{"id": 18557},
  {"id": 18918},{"id": 23814},{"id": 24851},{"id": 29033}
]

const URL = 'https://api.openweathermap.org/data/2.5/forecast';

class WeatherForecast {
  constructor (APIKey) {
    this.APIKey = APIKey;
  }

  async makeRequest(cityID) {
    try {
      const response = await fetch(`${URL}?id=${cityID}&appid=${this.APIKey}`
      )
      return await response.json();
    } catch(error) {
      console.error(error);
    }
  }
}

class RandomWeatherForecast extends WeatherForecast {
  constructor (APIKey, args) {
    super(APIKey);
    this.listOfCities = args;
  }

  makeRequest() {
    return super.makeRequest(this.listOfCities[this.getRandomInt(this.listOfCities.length)].id);
  }

  getRandomInt(maxValue) {
    return Math.floor(Math.random() * Math.floor(maxValue));
  }
}

const forecast = new WeatherForecast('0003867af3a31ba78e79574ac616112e');
forecast.makeRequest(18918)

const randomForecast = new RandomWeatherForecast('0003867af3a31ba78e79574ac616112e', CITIES);
randomForecast.makeRequest()
