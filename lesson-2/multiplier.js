// создает функцию, которая возвращает произведение y на x 
function multiplier(x) {
    return function(y) {
        return x * y;
    }
}