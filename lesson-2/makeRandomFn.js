/*
Cоздает функцию, которая возвращает случайный элемент
либо массива, либо списка аргументов
*/
function makeRandomFn(...args) {
  return function() {
      if (Array.isArray(...args)) {
          return args[0][getRandomInt(args[0].length)];
      } else {
          return args[getRandomInt(args.length)];
      }
  }
}
// возвращает случайно число в диапазоне [0,maxValue)
function getRandomInt(maxValue) {
  return Math.floor(Math.random() * Math.floor(maxValue));
}